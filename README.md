# gitea-custom

this repository tracks the customizations for [https://git.envs.net](https://git.envs.net).

feel free to use any or all parts of this.

## installing

if you want all of it, simply clone this in to your gitea root. otherwise, copy other files as needed!

example: gitea root is `/var/lib/gitea`:

```
cd /var/lib/gitea
git clone https://tildegit.org/envs/gitea-custom.git custom
```

make sure that you restart gitea afterwards.
